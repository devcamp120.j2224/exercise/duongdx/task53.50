package com.devcamp.j04_javabasic.s10;

import com.devcamp.j04_javabasic.s10.interfaceclass.ISdongvat;
import com.devcamp.j04_javabasic.s10.interfaceclass.ISwimable;

public class CFish extends CPet implements ISwimable,ISdongvat{
    private int size;
    private boolean canEat;
    public int getsize() {
        return size;
    }
    public void setsize(int size) {
        this.size = size;
    }
    public boolean getcanEat() {
        return canEat;
    }
    public void setcanEat(boolean canEat) {
        this.canEat = canEat;
    }

    @Override
    public void swim() {
        // TODO Auto-generated method stub
        System.out.println("Fist Swiming");
    }

    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mate() {
        // TODO Auto-generated method stub
        
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "CFish {\"age\":" + this.age + ", gender= " + this.gender + ", size= " + this.size + ", canEat= " + this.canEat + "}";
    }
    
}
