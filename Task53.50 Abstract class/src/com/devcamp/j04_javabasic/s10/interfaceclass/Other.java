package com.devcamp.j04_javabasic.s10.interfaceclass;

public interface Other {
    void other();
    int other(int param);
    String other(String param);
}
