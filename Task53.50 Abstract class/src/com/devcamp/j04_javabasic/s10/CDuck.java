package com.devcamp.j04_javabasic.s10;

import com.devcamp.j04_javabasic.s10.interfaceclass.ISkeukeu;
import com.devcamp.j04_javabasic.s10.interfaceclass.ISwimable;

public class CDuck extends CPet implements ISwimable,ISkeukeu{
    private int age;
    private String gender;
    private String beakColor;
    @Override
    public void tiengkeu() {
        System.out.println("quacquac");
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getgender() {
        return getGender();
    }
    public void setgender(String gender) {
        this.setGender(gender);
    }
    public String getbeakColor() {
        return beakColor;
    }
    public void setbeakColor(String beakColor) {
        this.beakColor = beakColor;
    }
    @Override
    public void swim() {
        
        
    }
    public String toString() {
        // TODO Auto-generated method stub
        return "CDuck {\"age\":" + this.age + ", gender= " + this.gender + ", beakcolor= " + this.beakColor + "}";
    }
}
