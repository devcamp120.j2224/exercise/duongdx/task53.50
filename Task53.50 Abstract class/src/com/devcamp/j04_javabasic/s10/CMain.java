package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CMain {
    public static void main(String[] args) {
        CPet dog = new CDog(3 , "mic");
        CPet cat = new CCat(4 , "meo");

        ArrayList<CPet> petslist = new ArrayList<>();
        petslist.add(dog);
        petslist.add(cat);

        CPerson person1 = new CPerson();
        CPerson person2 = new CPerson(2, 20 , "duong" , "dao", petslist);

        System.out.println("Fullname = " + person1.getFirstname());
        System.out.println("Fullname = " + person2.getFirstname() + " " + person2.getLastname());
        System.out.println("Age = " + person2.getAge());
        System.out.println(person2);
        ArrayList<Integer> myArrayList2 = new ArrayList<Integer>();
        myArrayList2.add(3);
        myArrayList2.add(2);
        myArrayList2.add(2);
        myArrayList2.add(1);
        CIntegerArrayList MyInter = new CIntegerArrayList();
       MyInter.setmIntegerArrayList(myArrayList2);
       String a =  MyInter.getSum();
        System.out.println(a);


        CDuck conVit = new CDuck();
        conVit.setAge(12);
        conVit.setGender("male");
        conVit.setbeakColor("vang");
        conVit.swim();
        conVit.tiengkeu();
        System.out.println(conVit);

        CFish ConCa = new CFish();
        ConCa.age = 31;
        ConCa.gender ="female";
        ConCa.setsize(20);
        ConCa.setcanEat(true);
        ConCa.eat();
        ConCa.swim();
        ConCa.isMammal();
        ConCa.mate();
        System.out.println(ConCa);
        
    }
}
